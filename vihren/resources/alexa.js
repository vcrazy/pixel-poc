/*
 * Script for getting alexa's info for sites
 */

var names = [], links = [], texts = [];

$.each($('.site-listing'), function(x, e){
	names.push($(e).find('a:first').text());
	links.push($(e).find('.topsites-label').text());

	$(e).find('.truncate .moreDesc:visible').click();
	$(e).find('.truncate').remove();
	$(e).find('.remainder').remove();

	texts.push($(e).find('.description').text());
});

names = JSON.stringify(names);
links = JSON.stringify(links);
texts = JSON.stringify(texts);
