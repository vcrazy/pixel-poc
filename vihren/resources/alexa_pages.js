var names_s = [], links_s = [], texts_s = []; // stringified and parsed
var names = [], links = [], texts = [];

function fill(n, l, t){
	for(var i in n){
		names.push(n[i]);
	}

	for(var i in l){
		links.push(l[i]);
	}

	for(var i in t){
		texts.push(t[i]);
	}
}

/*
 * Page 1
 */

names_s = JSON.parse('["NeoBux","Google","Facebook","Google","YouTube","Abv.bg","Zamunda.NET","Wikipedia","Видео споделяне","prodavalnik.com","Dir.bg","Blogspot.com","Yahoo!","Sportal.bg - The main source for sports information of all the Bulgarian media.","LinkedIn","Tumblr","jobs.bg","mobile.bg","Vesti.bg","blitz.bg","Дневник.bg","The Internet Movie Database","Dnes.bg","Adcash® Advertising Network","WordPress.com"]');
links_s = JSON.parse('["neobux.com","google.bg","facebook.com","google.com","youtube.com","abv.bg","zamunda.net","wikipedia.org","vbox7.com","prodavalnik.com","dir.bg","blogspot.com","yahoo.com","sportal.bg","linkedin.com","tumblr.com","jobs.bg","mobile.bg","vesti.bg","blitz.bg","dnevnik.bg","imdb.com","dnes.bg","adcash.com","wordpress.com"]');
texts_s = JSON.parse('["NeoBux excels in providing new business solutions as a Paid-to-Click service where users can multiply their earnings just by viewing advertisements. Advertisers benefit from the best quality/price ratio and the years of experience NeoBux has so that they can convert their advertising campaigns into successful ones.","Българската версия на търсеща машина.","A social utility that connects people, to keep up with friends, upload photos, share links and videos.","Enables users to search the world\'s information, including webpages, images, and videos. Offers unique features and search technology.","YouTube is a way to get your videos to the people who matter to you. Upload, tag and share your videos worldwide!","И-мейл сървър от България."," ","A free encyclopedia built collaboratively using wiki software. (Creative Commons Attribution-ShareAlike License).","Директория за споделяне на видео клипове, разделени по категории."," ","Интернет портал, предоставящ разнообразно информационно съдържание и услуги."," ","A major internet portal and service provider offering search results, customizable content, chatrooms, free e-mail, clubs, and pager.","Sportal.bg is the most popular sports media in Bulgaria. It employs over 40 journalist delivering over 300 daily news from all over the world. Currently the site reaches over 1 000 000 daily pageviews and is the first in Bulgaria to start its own web TV news service.","A networking tool to find connections to recommended job candidates, industry experts and business partners. Allows registered users to maintain a list of contact details of people they know and trust in business.","A feature rich and free blog hosting platform offering professional and fully customizable templates, bookmarklets, photos, mobile apps, and social network integration.","Портал за търсене и предлагане на работа и курсове в различни сфери.","Сайт No:1 за свободно публикуване на автомобилни обяви. Mястото, където се срещат търсещите и предлагащи автомобили.","Портален сайт с новини, спорт, право, кино, забавления и справочник."," ","Дневник.bg е сайтът на бизнес всекидневника Дневник. Обновява се постоянно с новините от България и света и с последната икономическа информаци","Features plot summaries, reviews, cast lists, and theatre schedules.","Новинарски сайт за новини от България и света на теми - спорт, технологии, социално-културни, здраве, криминалистика, автомобили, политика, бизнес и развлечения.","Adcash® is an international Ad network providing Internet publishers with profitable traffic monetization solutions and brand advertisers with premium audience reach. You can increase your ad revenues today by monetizing or advertising your website with Adcash®!","Free blogs managed by the developers of the WordPress software. Includes custom design templates, integrated statistics, automatic spam protection and other features."]');

fill(names_s, links_s, texts_s);

/*
 * Page 2
 */

names_s = JSON.parse('["Бг-Мама","media.tumblr.com","Arenabg.com","sinoptik.bg","eBay UK","zamunda.se","ibox.bg - Ibox.bg",": Gong : ","eBay","akamaihd.net","iNews.bg","xHamster\'s Free Porn Videos","Conduit","Twitter","24chasa.bg","Clixsense.com","sportni.bg","Гювеч","imot.bg","offnews.bg","Predpriemach.com","Pinterest","ХотНюз.БГ","googleusercontent.com","Mail.bg"]');
links_s = JSON.parse('["bg-mamma.com","media.tumblr.com","arenabg.com","sinoptik.bg","ebay.co.uk","zamunda.se","ibox.bg","gong.bg","ebay.com","akamaihd.net","inews.bg","xhamster.com","conduit.com","twitter.com","24chasa.bg","clixsense.com","sportni.bg","gbg.bg","imot.bg","offnews.bg","predpriemach.com","pinterest.com","hotnews.bg","googleusercontent.com","mail.bg"]');
texts_s = JSON.parse('["За бъдещите от настоящите майки и татковци, за бременността, раждането и осиновяването, първите месеци и години от живота на бебето. Голям форум където може да се потърси и получи съвет."," "," "," ","Person to person online auction site where you can buy or sell new and used items."," "," "," ","International person to person auction site, with products sorted into categories."," ","Новинарски сайт с вътрешни, международни и спортни новини.","xHamster is a community of open-minded people. Our site contains adult videos, photos, dating, flash games and live sex cams.","Free customizable toolbar to stay connected to users even when they are surfing other websites.","Social networking and microblogging service utilising instant messaging, SMS or a web interface."," ","ClixSense is an industry proven method that allows advertisers of every size or budget to direct targeted and unique traffic to their website. We also offer the ability for the average internet user to create a substantial monthly income by completing Offers, Surveys and Tasks. Along with our very generous affiliate program, thousands are earning weekly and monthly commissions with us. ClixSense offers advertising to every web merchant whether you are a start up business with a minimal advertising budget or if your business is already established but you are desiring additional web exposure. For as little as $2.40 you can position your website to be exposed to potential thousands of customers. Our industry leading software virtually eliminates any type of Internet bots that automatically click through to your site. Check it out today, you\'ll be glad you did!"," ","Български портал, организиран под мотото \'Всичко българско накуп\'.","База данни от оферти за продажби, наеми и ново строителство. Полезни връзки, новини и публикуване на частни и фирмени обяви."," ","Форум за онлайн бизнес, интернет реклама и оптимизация за търсачки.","Pinterest is an online pinboard: a place where you can post collections of things you love, and “follow” collections created by people with great taste.","Светските новини. Гореща информация за живота на българските и холивудските звезди. Актуални клюки, новини и видео материали. Скандали, слухове и пикантни истории."," ","Безплатен email адрес с размер 10GB, SMS известяване за пристигане на ново писмо, анти спам защита, безплатно POP3, антивирусна проверка."]');

fill(names_s, links_s, texts_s);

/*
 * Page 3
 */

names_s = JSON.parse('["Tyxo","Нова телевизия","cars.bg","Stack Overflow","Pornhub.com","Grabo.bg","subsunacs.net","Amazon.com","probux.com","Microsoft Corporation","SuperHosting.BG - Хостинг и регистрация на домейни от СуперХостинг.БГ - хостинг решения в България","fakti.bg","bTV","Капитал","sab.bz","xvideos.com","DARIK News - , , , ","Investor.bg","zajenata.bg","BG Maps","PayPal","Mtel.bg","Windows Live","Data.bg","Novini.bg"]');
links_s = JSON.parse('["tyxo.bg","novatv.bg","cars.bg","stackoverflow.com","pornhub.com","grabo.bg","subsunacs.net","amazon.com","probux.com","microsoft.com","superhosting.bg","fakti.bg","btv.bg","capital.bg","sab.bz","xvideos.com","dariknews.bg","investor.bg","zajenata.bg","bgmaps.com","paypal.com","mtel.bg","live.com","data.bg","novini.bg"]');
texts_s = JSON.parse('["Брояч и каталог на българските сайтове.","Като част от шведската медийна група MTG, Нова телевизия е втората частна национална телевизионна медия у нас.","Сайтът предлага buyback и употребявани автомобили от официални вноcители за България.","A language-independent collaboratively edited question and answer site for programmers. Questions and answers displayed by user votes and tags.","Please contact dice[/at/]pornhub[/dot/]com for any inquiries.","Grabo.bg намира в твоя град топ-офертата на деня, всеки ден, с отстъпки от 50% до 90%."," ","Amazon.com seeks to be Earth\'s most customer-centric company, where customers can find and discover anything they might want to buy online, and endeavors to offer its customers the lowest possible prices. Site has numerous personalization features and services including one-click buying, extensive customer and editorial product reviews, gift registries, gift certificates, wish lists, restaurant and movie listings, travel, and photo processing."," ","Main site for product information, support, and news.","Хостинг и регистрация на домейни от СуперХостинг.БГ! Надеждни хостинг решения за вашият Интернет сайт в България! Безплатна поддръжка 24/7! Х"," ","Частна национална телевизия, собственост на Нюз Корпорейшън.","Национален бизнес и политически седмичник."," "," "," ","Предоставя информация от България и света - новини, анализи, котировки, графики, коефициенти и прогнози. Популярен форум с икономическа тематика."," ","Search for Bulgarian settlements, streets, routes, or resorts. Designed for use on computers, cell phones, and PDA\'s. A Datecs Ltd - GIS Center site.","Online payment service for individuals and merchants. Allows users to send money and bills to anyone with e-mail.","Mobiltel EAD Human Resources Division 1 Kukush Street, 1309 Sofia jobs@mobiltel.bg , www.mtel.bg","Search engine from Microsoft."," ","Новини и информация за най-важните събития в сферата на политиката, шоубизнеса, спорта и технологиите."]');

fill(names_s, links_s, texts_s);

/*
 * Page 4
 */

names_s = JSON.parse('["Bing","Гепиме.ком","9Gag","jenite.bg","LiveScore","vube.com","Profit.Bg","Стандарт нюз","Goodgame Studios - Altigi GmbH","btvnews.bg","pik.bg","gotvach.bg","Kaldata.com","bg53.com","Елмаз","Booking.com","Svejo","Republic of Bulgaria","playmillion2.com","ask.fm","thepiratebay.sx","ezine.bg","div.bg","Apple Inc.","MSN"]');
links_s = JSON.parse('["bing.com","gepime.com","9gag.com","jenite.bg","livescore.com","vube.com","profit.bg","standartnews.com","goodgamestudios.com","btvnews.bg","pik.bg","gotvach.bg","kaldata.com","bg53.com","elmaz.com","booking.com","svejo.net","government.bg","playmillion2.com","ask.fm","thepiratebay.sx","ezine.bg","div.bg","apple.com","msn.com"]');
texts_s = JSON.parse('["Search engine developed by Microsoft. Features web, image, video, local, news, and product search.","Необвързващи запознанства онлайн.","User generated funny pictures and videos with commenting and ratings. Content can be sorted according to popularity."," ","Real time football (soccer) match scores across International and over 100 leagues in 34 countries on a permanent basis."," ","Provides financial and economical news from Bulgaria and Region.","Информационна агенция Стандарт Нюз и вестник Стандарт - новини, анализи, коментари, интервюта за икономика, бизнес, общество, политика, технологии, спорт, култура, стил, здраве.","Das Unternehmen ist spezialisiert auf die Entwicklung von browserbasierten Online-Spielen und Social Games und informiert über die Produkte, die Firma, Jobangebote, Partnerangebote, Presseinformationen, Neuigkeiten und Kundensupport. [D-22761 Hamburg]"," "," "," ","Софтуер, хардуер, програмиране, ОС, интернет търговия."," ","Възможности за нови запознанства.","Worldwide accommodation reservations.","Social Media Network","Official government site tells about the government and the country in general. Includes the history of the country, some basic statistics, national symbols, public holidays, and a map of the districts as well as speeches by the prime minister and other government news and decrees."," "," "," "," "," ","Official site, with details of products and services.","Portal for shopping, news and money, e-mail, search, and chat."]');

fill(names_s, links_s, texts_s);

/*
 * Exported
 */

for(var i in names){
	console.log(names[i]);
	console.log(links[i]);
	console.log(texts[i]);
}
