try {
	var pageTracker = _gat._getTracker("UA-44231572-1");
	pageTracker._trackPageview();
} catch(err) {}

$(document).ready(function(){
	$('.info-container').click(function(){
		var label = {
			x: $(this).data('x'),
			y: $(this).data('y'),
			id: $(this).data('id')
		},
			stringified = '{"x":' + label.x + ',"y":' + label.y + ',"id":"' + label.id + '"}';

//		pageTracker._trackEvent(category, action, opt_label, opt_value, opt_noninteraction)
		pageTracker._trackEvent('Click: x',  'Click: ' + label.x,  stringified, 1, true);
		pageTracker._trackEvent('Click: y',  'Click: ' + label.y,  stringified, 1, true);
		pageTracker._trackEvent('Click: id', 'Click: ' + label.id, stringified, 1, false);

//		console.log(stringified);
	});
});
