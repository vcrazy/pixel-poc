<?php

	$names = array();
	$links = array();
	$texts = array();
	$map = array();

	$handle = fopen('info.txt', 'rb');
	$contents = stream_get_contents($handle);
	fclose($handle);

	$contents = explode(PHP_EOL, $contents);

	$indexes = range(0, 99);
	shuffle($indexes);

	while(($index = array_pop($indexes)) || $index === 0)
	{
		$name = $contents[$index * 3];
		$link = $contents[$index * 3 + 1];
		$text = trim($contents[$index * 3 + 2]);

		$names[] = htmlspecialchars($name);
		$links[] = htmlspecialchars($link);
		$texts[] = $text ? htmlspecialchars($name) . ': ' . htmlspecialchars($text) : htmlspecialchars($name);
		$map[] = $index;
	}

	