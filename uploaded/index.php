<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Най-посещаваните сайтове в България</title>
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.12.0/build/cssreset/cssreset-min.css" />
	<link rel="stylesheet" href="styles.css" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="http://www.google-analytics.com/ga.js" type="text/javascript"></script>
	<script src="scripts.js" type="text/javascript"></script>
</head>
<body>
<div id="container">
	<?php
		require_once('get_info.php');

		foreach(array_keys($names) as $index):
		if($index % 10 == 0): ?><div class="liner"><?php endif;
	?><div class="info-container" data-id="<?php echo $links[$index]; ?>" data-x="<?php echo $index % 10 + 1; ?>" data-y="<?php echo floor($index / 10) + 1; ?>"><a href="<?php echo $links[$index]; ?>" target="_blank" title="<?php echo $names[$index]; ?>"><img src="resources/<?php echo $map[$index]; ?>.png" title="<?php echo $texts[$index]; ?>" alt="<?php echo $texts[$index]; ?>" /></a></div><?php
		if($index % 10 == 9): ?></div><?php endif;
		endforeach;
	?>
</div>
</body>
</html>